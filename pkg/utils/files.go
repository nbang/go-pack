package utils

import (
	"io/ioutil"
	"os"
)

func TouchFile(name string) error {
	file, err := os.OpenFile(name, os.O_RDONLY|os.O_CREATE, 0644)
	if err != nil {
		return err
	}
	return file.Close()
}

func ReadFile(path string) []byte {
	// init aws region
	data, err := ioutil.ReadFile(path)
	HandleError(err)
	return data
}
