package utils

import (
	"encoding/json"

	"github.com/google/uuid"
)

func StructToStrJson(v interface{}) string {
	b, _ := json.Marshal(v)
	return string(b)
}

func RandomUUID() string {
	uid, err := uuid.NewRandom()
	HandleError(err)
	return uid.String()
}

func StringToUUID(s string) string {
	uid := uuid.NewSHA1(uuid.NameSpaceOID, []byte(s))
	return uid.String()
}
