package logger

import (
	"os"
	"sync"
)

var (
	mulock      sync.RWMutex
	level       Level
	encoding    = "json"
	stringLevel = "debug"
	logger      StandardLogger
)

func init() {
	mulock.Lock()
	defer mulock.Unlock()
	logger = &SugaredLogger{}
	loglevel := os.Getenv("LOG_LEVEL")
	if len(loglevel) != 0 {
		stringLevel = loglevel
	}
	logger.InitLog(stringLevel, encoding)
	level.UnmarshalText([]byte(stringLevel))
}

// Debug logs a message at level Info on the standard logger.
func Debug(args ...interface{}) {
	logger.Debug(args...)
}

// Debugf logs a message at level Info on the standard logger.
func Debugf(template string, args ...interface{}) {
	logger.Debugf(template, args...)
}

// Info logs a message at level Info on the standard logger.
func Info(args ...interface{}) {
	logger.Info(args...)
}

// Infof logs a message at level Info on the standard logger.
func Infof(template string, args ...interface{}) {
	logger.Infof(template, args...)
}

// Warn logs a message at level Warn on the standard logger.
func Warn(args ...interface{}) {
	logger.Warn(args...)
}

// Warnf logs a message at level Warn on the standard logger.
func Warnf(template string, args ...interface{}) {
	logger.Warnf(template, args...)
}

// Error logs a message at level Error on the standard logger.
func Error(args ...interface{}) {
	logger.Error(args...)
}

// Errorf logs a message at level Error on the standard logger.
func Errorf(template string, args ...interface{}) {
	logger.Errorf(template, args...)
}

// Panic logs a message at level Panic on the standard logger.
func Panic(args ...interface{}) {
	logger.Panic(args...)
}

// Panicf logs a message at level Panic on the standard logger.
func Panicf(template string, args ...interface{}) {
	logger.Panicf(template, args...)
}

// Fatal logs a message at level Fatal on the standard logger then the process will exit with status set to 1.
func Fatal(args ...interface{}) {
	logger.Fatal(args...)
}

// Fatalf logs a message at level Fatal on the standard logger then the process will exit with status set to 1.
func Fatalf(msg string, args ...interface{}) {
	logger.Fatalf(msg, args...)
}

// SetLevel set log lever
func SetLevel(strLevel string) {
	mulock.Lock()
	defer mulock.Unlock()

	logger.InitLog(strLevel, encoding)
	level.UnmarshalText([]byte(strLevel))
}

// GetLevel get log lever
func GetLevel() Level {
	return level
}
