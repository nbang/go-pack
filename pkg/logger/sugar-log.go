package logger

import (
	"encoding/json"
	"fmt"

	"go.uber.org/zap"
)

type SugaredLogger struct {
	logger *zap.SugaredLogger
}

func (log *SugaredLogger) InitLog(logLevel, encoding string) {
	rawJSON := []byte(fmt.Sprintf(`{
	  "level": "%s",
	  "encoding": "%s",
	  "outputPaths": ["stdout"],
	  "errorOutputPaths": ["stderr"],
	  "encoderConfig": {
	    "messageKey": "message",
	    "levelKey": "level",
        "timeKey": "@timestamp",
		"timeEncoder": "ISO8601",
	    "levelEncoder": "lowercase",
		"durationEncoder": "1000"
	  }
	}`, logLevel, encoding))

	var cfg zap.Config
	if err := json.Unmarshal(rawJSON, &cfg); err != nil {
		panic(err)
	}

	logger, err := cfg.Build()
	if err != nil {
		panic(err)
	}

	log.logger = logger.Sugar()
}

// Debug logs a message at level Info on the standard logger.
func (log *SugaredLogger) Debug(args ...interface{}) {
	log.logger.Debug(args...)
}

// Debugf logs a message at level Info on the standard logger.
func (log *SugaredLogger) Debugf(template string, args ...interface{}) {
	log.logger.Debugf(template, args...)
}

// Info logs a message at level Info on the standard logger.
func (log *SugaredLogger) Info(args ...interface{}) {
	log.logger.Info(args...)
}

// Infof logs a message at level Info on the standard logger.
func (log *SugaredLogger) Infof(template string, args ...interface{}) {
	log.logger.Infof(template, args...)
}

// Warn logs a message at level Warn on the standard logger.
func (log *SugaredLogger) Warn(args ...interface{}) {
	log.logger.Warn(args...)
}

// Warnf logs a message at level Warn on the standard logger.
func (log *SugaredLogger) Warnf(template string, args ...interface{}) {
	log.logger.Warnf(template, args...)
}

// Error logs a message at level Error on the standard logger.
func (log *SugaredLogger) Error(args ...interface{}) {
	log.logger.Error(args...)
}

// Errorf logs a message at level Error on the standard logger.
func (log *SugaredLogger) Errorf(template string, args ...interface{}) {
	log.logger.Errorf(template, args...)
}

// Panic logs a message at level Panic on the standard logger.
func (log *SugaredLogger) Panic(args ...interface{}) {
	log.logger.Panic(args...)
}

// Panicf logs a message at level Panic on the standard logger.
func (log *SugaredLogger) Panicf(template string, args ...interface{}) {
	log.logger.Panicf(template, args...)
}

// Fatal logs a message at level Fatal on the standard logger then the process will exit with status set to 1.
func (log *SugaredLogger) Fatal(args ...interface{}) {
	log.logger.Fatal(args...)
}

// Fatalf logs a message at level Fatal on the standard logger then the process will exit with status set to 1.
func (log *SugaredLogger) Fatalf(msg string, args ...interface{}) {
	log.logger.Fatalf(msg, args...)
}
