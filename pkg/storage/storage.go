package storage

import (
	"fmt"
	"go-pack/pkg/storage/plugins"
	"sync"

	"github.com/go-pg/pg/v9"
	"github.com/jinzhu/gorm"
)

type Storage interface {
	SetConfigs(map[string]string)
	Connect() error
	Close() error
	GetPsqlInstance() *pg.DB
	GetMssqlInstance() *gorm.DB
}

var storageMap map[string]Storage
var storageLock sync.Mutex

func init() {
	storageMap = make(map[string]Storage)
	RegisterStorage(plugins.PSQL_STORAGE, &plugins.PsqlStorage{})
	RegisterStorage(plugins.MSSQL_STORAGE, &plugins.MssqlStorage{})
}

func RegisterStorage(encType string, storage Storage) {
	storageLock.Lock()
	defer storageLock.Unlock()
	storageMap[encType] = storage
}

func storageForType(storage string) Storage {
	storageLock.Lock()
	defer storageLock.Unlock()
	return storageMap[storage]
}

func NewStorage(storageType string) (Storage, error) {
	storage := storageForType(storageType)

	if storage == nil {
		return nil, fmt.Errorf("No storage driver for %s ", storageType)
	}

	return storage, nil
}
