package plugins

import (
	"fmt"
	"go-pack/pkg/utils"
	"strconv"
	"time"

	"github.com/go-pg/pg/v9"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mssql"
)

const MSSQL_STORAGE = "mssql"

type MssqlStorage struct {
	db                     *gorm.DB
	username               string
	password               string
	host                   string
	database               string
	encrypt                string
	trustServerCertificate string
	poolSize               string
	hostNameInCertificate  string
	loginTimeout           string
}

var (
	err error
)

func (s *MssqlStorage) SetConfigs(configs map[string]string) {
	s.username = configs["username"]
	s.password = configs["password"]
	s.host = configs["host"]
	s.database = configs["database"]
	s.poolSize = configs["poolSize"]
	s.encrypt = configs["encrypt"]
	s.trustServerCertificate = configs["trustServerCertificate"]
	s.hostNameInCertificate = configs["hostNameInCertificate"]
	s.loginTimeout = configs["loginTimeout"]
}

func (s *MssqlStorage) Connect() error {
	cns := s.getConnectionString()

	s.db, err = gorm.Open("mssql", cns)
	pool, _ := strconv.Atoi(s.poolSize)
	s.db.DB().SetConnMaxLifetime(20 * time.Minute)
	s.db.DB().SetMaxIdleConns(pool)
	s.db.DB().SetMaxOpenConns(pool)
	return err
}

func (s *MssqlStorage) GetMssqlInstance() *gorm.DB {
	return s.db
}

func (s *MssqlStorage) GetPsqlInstance() *pg.DB {
	return nil
}

func (s *MssqlStorage) Close() error {
	err := s.db.Close()
	return utils.LogError(err)
}

func (s *MssqlStorage) getConnectionString() string {
	return fmt.Sprintf("sqlserver://%s:%s@%s?database=%s;encrypt=%s;"+
		"trustServerCertificate=%s;hostNameInCertificate=%s;loginTimeout=%s;Poolsize=%s",
		s.username, s.password, s.host, s.database, s.encrypt, s.trustServerCertificate,
		s.hostNameInCertificate, s.loginTimeout, s.poolSize)
}
