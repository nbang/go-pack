package plugins

import (
	"go-pack/pkg/utils"
	"strconv"

	"github.com/go-pg/pg/v9"
	"github.com/jinzhu/gorm"
)

const PSQL_STORAGE = "postgres"

type PsqlStorage struct {
	db       *pg.DB
	username string
	password string
	host     string
	database string
	schema   string
	poolSize int
}

func (s *PsqlStorage) SetConfigs(configs map[string]string) {
	s.username = configs["username"]
	s.password = configs["password"]
	s.host = configs["host"]
	s.database = configs["database"]
	if v, ok := configs["schema"]; ok {
		s.schema = v
	} else {
		s.schema = "public"
	}
	s.poolSize, _ = strconv.Atoi(configs["poolSize"])
}

func (s *PsqlStorage) Connect() error {
	s.db = pg.Connect(&pg.Options{
		User:     s.username,
		Password: s.password,
		Addr:     s.host,
		Database: s.database,
		PoolSize: s.poolSize,
		OnConnect: func(conn *pg.Conn) error {
			_, err := conn.Exec("set search_path=?", s.schema)
			if err != nil {
				_ = utils.LogError(err)
			}
			return nil
		},
	})

	return nil
}

func (s *PsqlStorage) Close() error {
	err := s.db.Close()
	return utils.LogError(err)
}

func (s *PsqlStorage) GetPsqlInstance() *pg.DB {
	if s != nil {
		return s.db
	}
	return nil
}

func (s *PsqlStorage) GetMssqlInstance() *gorm.DB {
	return nil
}
