module go-pack

go 1.14

require (
	github.com/go-pg/pg/v9 v9.2.0
	github.com/google/uuid v1.1.2
	github.com/jinzhu/gorm v1.9.16
	go.uber.org/zap v1.16.0
)
